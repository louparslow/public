# DORA metrics

The DORA metrics, derived from the research presented in the book 'Accelerate', offer a set of standardized performance indicators for technology teams. They encompass four critical aspects: 

1. **Deployment Frequency**: How often an organization releases code to production or a live environment.
2. **Lead Time for Changes**: The duration from code being committed until it's effectively deployed in production.
3. **Time to Restore Service**: The time it takes to recover from an incident or outage and restore service to normal.
4. **Change Fail Rate**: The percentage of changes that fail or cause issues in the live environment.

Together, these metrics provide a comprehensive view of software delivery and operational performance, helping IT teams identify areas for improvement and drive excellence in their processes."

## Accelerate: The Science of Lean Software and DevOps: Building and Scaling High Performing Technology Organizations" by Nicole Forsgren, Jez Humble, and Gene Kim

![Accelerate](https://learning.oreilly.com/library/cover/9781457191435/250w/)

Accelerate dives deep into the world of DevOps, presenting research-backed strategies for achieving high performance in technology organizations. Central to their findings are the DORA metrics, which focus on deployment frequency, lead time for changes, time to restore service, and change fail rate. These metrics offer a clear, data-driven lens to view and improve software delivery processes.

Video [Accelerate authors: The Data Behind DevOps: Becoming a High Performer -DORA](https://youtu.be/DgpsX5yLXQw?si=-QICfz8evK4zF-Br)